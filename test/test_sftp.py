import os
import json

from unittest import TestCase

from pecutil.sftp import load_key, walk_sftp, get_transport, copy_directory, copy_folder

__author__ = 'jbui'


class TestSFTP(TestCase):

    def setUp(self):

        self.test_folder = os.path.abspath(os.path.dirname(__file__))

        with open('vader.json', 'r') as data_file:
            data = json.loads(data_file.read())

        self.HOSTNAME = data['hostname']
        self.USERNAME = data['username']
        self.PASSWORD = data['password']
        self.PORT = data['port']

        self.key, self.key_type = load_key(r'C:\Users\jbui.PEC\.ssh\known_hosts', self.HOSTNAME)
        self.sftp = get_transport(self.HOSTNAME, self.PORT, self.USERNAME, self.PASSWORD, self.key)

    def test_walk_file(self):
        remote_dir = walk_sftp(self.sftp, r'TEST')

        print(remote_dir)

        self.assertEqual(remote_dir['directory'], 'TEST')

        self.assertEqual(remote_dir['folders'][0]['directory'], 'TEST/CMU')

        self.assertEqual(len(remote_dir['folders'][0]['files']), 13)

    def test_copy_file(self):
        remote_keys = walk_sftp(self.sftp, r'TEST/CMU')

        dump_folder = os.path.join(self.test_folder, 'DUMP\\TRIAL\\')

        copy_folder(self.sftp, remote_keys, dump_folder)

        self.assertTrue(os.path.isdir(dump_folder))
