import numpy as np

__author__ = 'jbui'


def check_empty(value):
    if isinstance(value, np.ndarray):
        return value.any()
    else:
        return value is not []
